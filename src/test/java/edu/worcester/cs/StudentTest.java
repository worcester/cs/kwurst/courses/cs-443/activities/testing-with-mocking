package edu.worcester.cs;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class StudentTest {
	
	Student student1;

	@BeforeEach
	void setUp() {
		student1 = new Student("Sue", "Storm");
	}

	@Test
	void testGetStudentCount() {
		int countBefore = Student.getStudentCount();
		new Student("Johhny", "Storm");
		assertEquals(Student.getStudentCount(), countBefore + 1);
	}

	@Test
	void testGetId() {
		int countBefore = Student.getStudentCount();
		Student student2 = new Student("Johhny", "Storm");
		assertEquals(student2.getId(), String.format("%07d", countBefore + 1));
	}

	@Test
	void testSetFirstName() {
		student1.setFirstName("Johnny");
		assertEquals("Johnny", student1.getFirstName());
	}

	@Test
	void testGetFirstName() {
		assertEquals("Sue", student1.getFirstName());
	}

	@Test
	void testSetLastName() {
		student1.setLastName("Richards");
		assertEquals("Richards", student1.getLastName());
	}

	@Test
	void testGetLastName() {
		assertEquals("Storm", student1.getLastName());
	}

}
