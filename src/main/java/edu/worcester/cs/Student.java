package edu.worcester.cs;

public class Student{
	  private String firstName, lastName, id;     
	  private static int nextId = 1;
	    
	  public Student(String firstName,
	                 String lastName) {        
	    this.firstName = firstName;        
	    this.lastName = lastName;        
	    this.id = String.format("%07d",nextId);
	    nextId++;     
	  }    

	  public static int getStudentCount() {        
	    return nextId-1;    
	  }

	  public String getId() {        
	    return id;    
	  }

	  public void setFirstName(String firstName) {
	    this.firstName = firstName;    
	  }    

	  public String getFirstName() {        
	    return firstName;    
	  }

	  public void setLastName(String lastName) {        
	    this.lastName = lastName;    
	  }

	  public String getLastName() {        
	    return lastName;    
	  }
	} 
